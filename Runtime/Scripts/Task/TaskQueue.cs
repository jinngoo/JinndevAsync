﻿using System;
using System.Collections;
using System.Threading.Tasks;

namespace Jinndev.Async {

    public abstract class TaskQueue {

        public string description;

        public TaskQueue(string description) {
            this.description = description;
        }

        public abstract bool HasNext();
        public abstract Task InvokeNextAsync();
        public abstract void InvokeNext();
        public abstract float GetProgress();
        public abstract int GetCount();

    }

}
