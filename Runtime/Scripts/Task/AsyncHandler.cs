﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Runtime.CompilerServices;
//using System.Threading.Tasks;
//using UnityEngine;

//namespace Jinndev.Async {

//    public class AsyncHandler : MonoBehaviour {

//        private static AsyncHandler instance;
//        public static AsyncHandler Instance {
//            get {
//                if (instance == null) {
//                    GameObject obj = new GameObject("AsyncHandler");
//                    DontDestroyOnLoad(obj);

//                    instance = obj.AddComponent<AsyncHandler>();
//                }
//                return instance;
//            }
//        }

//        public uint frame;

//        private void Update() {
//            frame++;
//        }

//        private void OnDestroy() {
//            instance = null;
//        }

//    }

//    public struct EndOfFrameAwaiter : INotifyCompletion {

//        public string name;
//        public uint frame;

//        public EndOfFrameAwaiter(string name, uint frame) {
//            this.name = name;
//            this.frame = frame;
//        }

//        public void OnCompleted(Action continuation) {
//            Debug.Log(name+ ".OnCompleted()");
//            continuation.Invoke();
//        }

//        public bool IsCompleted {
//            get {
//                Debug.Log(name+ ".IsCompleted");
//                return false;
//                //return AsyncHandler.Instance.frame - frame >= 3;
//            }
//        }

//        public uint GetResult() {
//            Debug.Log(name + ".GetResult()");
//            return frame;
//        }

//    }

//    public struct EndOfFrame {

//        public string name;
//        public EndOfFrame(string name) {
//            this.name = name;
//        }

//        public TaskAwaiter GetAwaiter() {
//            return Task.Delay(1).GetAwaiter();
//            //return new EndOfFrameAwaiter(name, AsyncHandler.Instance.frame);
//        }

//    }

//}